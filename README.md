﻿# README #
uso livre

Lista branca para ufw do ubuntu

### What is this repository for? ###

Este repositorio contem listas permissivas dos IPs do Brasil.
Ela são usadas para sites hospedados em distribuição linux ubunto, ou outra que use 
ufw
https://help.ubuntu.com/community/UFW

### How do I get set up? ###

Para usar escolha uma das lista ex:

lista_branca_ubuntu_firewall_ufw_protocolo_tcp_porta_80.txt

use o comando sh do linux:

ubuntu > sh lista_branca_ubuntu_firewall_ufw_protocolo_tcp_porta_80.txt
ou 
colar no terminal na conta root


Você tambem pode adicionar as regras no arquivo 
/etc/ufw/user.rules

usando as regras compiladas:
80_regras.txt
443_regras.txt
22_regras.txt
222_regras.txt

ou usar o  já com as regras inseridas user.rules


tenha cuidado ao usar a lista "lista_branca_ubuntu_firewall_ufw_porta_22.txt"
para não fechar seu acesso ao ssh

#
### Who do I talk to? ###

autor: jorge sales
email:octal@ig.com.br